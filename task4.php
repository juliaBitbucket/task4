<?php
$city = 'Kiev,ua';
$appid = '66af55553cc0d118c25bd0e6bc05b7f6';
$cacheFile = __DIR__ . '/cache.json';
$updateCacheTime = 3600; // second

if(filemtime($cacheFile) === false || (time() - filemtime($cacheFile)) > $updateCacheTime)
{
    $weather = json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/weather?q=$city&appid=$appid"), true);
    file_put_contents($cacheFile, json_encode($weather));
} else {
    $weather = json_decode(file_get_contents($cacheFile), true);
}
?>

<?php //echo file_get_contents("http://api.openweathermap.org/data/2.5/weather?q=$city&mode=html&appid=$appid"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Weather</title>
        <style type="text/css">
            .option {
                color: gray;
                font-size: large;
                clear: left;
            }
            #picture {
                height: 45px;
                width: 50px;
                border: medium none;
                width: 50px;
                height: 45px;
                background: url("http://openweathermap.org/img/w/<?= $weather['weather'][0]['icon'] ?>.png") repeat scroll 0% 0% transparent;
            }
        </style>
        <meta charset="utf-8">
    </head>
    <body>
        <h1><?= $weather->name ?></h1>
        <div style="margin-left: 5px;">
            <div style="float: left;">
                <img id="picture" src="http://openweathermap.org/images/transparent.png"/>
            </div>
            <div class="option" >Temperature:<?= (isset($weather['main']['temp']) ? $weather['main']['temp'] - 273.15 : 'N/a')?>°C</div>
            <div class="option" >Clouds: <?= (isset($weather['clouds']['all']) ? $weather['clouds']['all'] : 'N/a') ?>%</div>
            <div class="option" >Humidity: <?= (isset($weather['main']['humidity']) ? $weather['main']['humidity'] : 'N/a') ?>%</div>
            <div class="option" >Wind: <?= (isset($weather['wind']['speed']) ? $weather['wind']['speed'] : 'N/a') ?> m/s</div>
            <div class="option" >Wind direction, degrees: <?= (isset($weather['wind']['deg']) ? $weather['wind']['deg'] : 'N/a') ?></div>
            <div class="option" >Pressure: <?= (isset($weather['main']['pressure']) ? $weather['main']['pressure'] : 'N/a') ?>hPa</div>
            <div class="option" >Atmospheric pressure on the sea level: <?= (isset($weather['main']['sea_level']) ? $weather['main']['sea_level'] : 'N/a ') ?>hPa</div>
            <div class="option" >Atmospheric pressure on the ground level: <?= (isset($weather['main']['grnd_level']) ? $weather['main']['grnd_level'] : 'N/a ') ?>hPa</div>
        </div>
    </body>
</html>
